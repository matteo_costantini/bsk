package bsk;

import static org.junit.Assert.*;
import org.junit.Test;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void firstThrowShouldBe1() throws Exception{
		Frame frame = new Frame(1,4);
		assertEquals(1,frame.getFirstThrow());
	}
	
	@Test
	public void secondThrowShouldBe1() throws Exception{
		Frame frame = new Frame(5,1);
		assertEquals(1,frame.getSecondThrow());
	}

	@Test
	public void scoreShouldBe6() throws Exception{
		Frame frame = new Frame(5,1);
		assertEquals(6,frame.getScore());
	}
}
