package bsk;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	private Game game;
	
	@Before
	public void setup() throws BowlingException {
		this.game = new Game();
	}
	
	@Test
	public void secondFrameShouldBe_3_6() throws BowlingException {
		this.game.addFrame(new Frame(1,5));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(2,6));
		
		assertEquals(3,this.game.getFrameAt(1).getFirstThrow());
		assertEquals(6,this.game.getFrameAt(1).getSecondThrow());
	}
	
	@Test
	public void gameScoreShouldBe81() throws BowlingException {
		this.game.addFrame(new Frame(1,5));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(2,6));
		
		assertEquals(81,this.game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe88() throws BowlingException {
		this.game.addFrame(new Frame(1,9));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(2,6));
		
		assertEquals(88,this.game.calculateScore());
	}

	@Test
	public void gameScoreShouldBe94() throws BowlingException {
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(2,6));
		
		assertEquals(94,this.game.calculateScore());
	}

	@Test
	public void gameScoreShouldBe103() throws BowlingException {
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(4,6));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(2,6));
		
		assertEquals(103,this.game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe112() throws BowlingException {
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(2,6));
		
		assertEquals(112,this.game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe98() throws BowlingException {
		this.game.addFrame(new Frame(8,2));
		this.game.addFrame(new Frame(5,5));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(2,6));
		
		assertEquals(98,this.game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe90() throws BowlingException {
		this.game.addFrame(new Frame(1,5));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(2,8));
		this.game.setFirstBonusThrow(7);
		
		assertEquals(90,this.game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe92() throws BowlingException {
		this.game.addFrame(new Frame(1,5));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(7,2));
		this.game.addFrame(new Frame(3,6));
		this.game.addFrame(new Frame(4,4));
		this.game.addFrame(new Frame(5,3));
		this.game.addFrame(new Frame(3,3));
		this.game.addFrame(new Frame(4,5));
		this.game.addFrame(new Frame(8,1));
		this.game.addFrame(new Frame(10,0));
		this.game.setFirstBonusThrow(7);
		this.game.setSecondBonusThrow(2);
		
		assertEquals(92,this.game.calculateScore());
	}
	
	@Test
	public void gameScoreShouldBe300() throws BowlingException {
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.addFrame(new Frame(10,0));
		this.game.setFirstBonusThrow(10);
		this.game.setSecondBonusThrow(10);
		
		assertEquals(300,this.game.calculateScore());
	}
}
