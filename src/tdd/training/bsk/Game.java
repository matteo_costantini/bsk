package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	private ArrayList<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<Frame>();
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(frames.size() == 10)
			throw new BowlingException("Game already have 10 frames");
		
		frames.add(frame);
		
		// Add bonus to previous frame
		if(frames.size() > 1)
		{
			if(frames.get(frames.size()-2).isStrike())
				frames.get(frames.size()-2).setBonus(frame.getFirstThrow() + frame.getSecondThrow());
			else if(frames.get(frames.size()-2).isSpare())
				frames.get(frames.size()-2).setBonus(frame.getFirstThrow());
		}
		
		// Add bonus for multiple strikes
		calculateMultipleStrikes();
	}

	private void calculateMultipleStrikes() {
		for(int i=0; i<frames.size()-1; i++) {
			int bonus = 0;
			boolean strike = false;
			if(frames.get(i).isStrike() && (i < frames.size()-1)) {
				strike = true;
				bonus += frames.get(i+1).getFirstThrow();
				bonus += frames.get(i+1).getSecondThrow();
			}
			if(strike && frames.get(i+1).isStrike()) {
				if(i < frames.size()-2)
					bonus += frames.get(i+2).getFirstThrow();
				frames.get(i).setBonus(bonus);
			}
		}
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		if(index < 0 || index > 9)
			throw new BowlingException("Frame index out of bounds");
		
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		for(Frame frame : frames) {
			score += frame.getFirstThrow();
			score += frame.getSecondThrow();
			score += frame.getBonus();
		}
		
		if(frames.get(frames.size()-1).isStrike()) {
			score += this.firstBonusThrow;
			score += this.secondBonusThrow;
		}
		else if(frames.get(frames.size()-1).isSpare())
			score += this.firstBonusThrow;
		
		if(frames.get(frames.size()-2).isStrike() && frames.get(frames.size()-1).isStrike())
			score += this.firstBonusThrow;
		
		return score;
	}

}
